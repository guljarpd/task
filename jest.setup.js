const server = require('./server');

beforeAll((done) => {
  done();
});
afterAll(async () => {
  // avoid jest open handle error
  await new Promise((resolve) => setTimeout(() => resolve(), 500));
  // close server
  server.closeServer();
});
