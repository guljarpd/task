const request = require('supertest');
const server = require('../server');

describe('Task testing begins for /task/create', () => {
  it('should create a new task /task/create', async () => {
    const res = await request(server).post('/task/create').send({
      task_name: 'Buy new home',
      status: 'OPEN',
      created_user_id: 1
    });
    expect(res.statusCode).toEqual(201);
    expect(res.body.success).toEqual(true);
  });

  it('should failed with insufficient params /task/create', async () => {
    const res = await request(server).post('/task/create').send({
      task_name: 'Buy new home',
      status: 'OPEN',
    });
    expect(res.statusCode).toEqual(400);
    expect(res.body.success).toEqual(false);
  });
});

describe('Task testing begins for /task/fetch', () => {
  it('should fetch list of task /task/fetch', async () => {
    const res = await request(server).get('/task/fetch?page_number=1&limit=3')
    expect(res.statusCode).toEqual(200);
    expect(res.body.success).toEqual(true);
  });
});

describe('Task testing begins for /task/update', () => {
  it('should update the task /task/update', async () => {
    const res = await request(server).put(`/task/update/5`).send({
      status: 'IN_PROGRESS',
    });
    expect(res.statusCode).toEqual(200);
    expect(res.body.success).toEqual(true);
  });
});

describe('Task testing begins for /task/fetch/metrics', () => {
  it('should fetch the task metrics  /task/fetch/metrics', async () => {
    const res = await request(server).get('/task/fetch/metrics')
    expect(res.statusCode).toEqual(200);
    expect(res.body.success).toEqual(true);
  });
});
