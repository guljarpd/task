require("dotenv").config();
const express = require("express");
const cors = require("cors");
const { constants } = require("./configs");
const routes = require("./src/routes");
const db = require("./src/models");
const app = express();
app.use(express.json());
app.use(cors());
routes(app);

// db connection inti
db.sequelize
  .authenticate()
  .then((_res) => {
    console.log("DB connection Success=>", "Database successfully connected");
  })
  .catch((err) => {
    console.error("DB connection Error=>", err);
  });

// health check api
app.get("/", (_req, res) => {
  return res.status(200).json({
    status: true,
    message: "Welcome to TaskApi",
  });
});

// start server
const server = app.listen(constants.PORT, () => {
  console.log(`Server stared on port: ${constants.PORT}`);
});

// close server Graceful
app.closeServer = () => {
  server.close();
};

module.exports = app;
