// add all constants here
const PORT = process.env.PORT;
const DATABASE_CREDS = {
    NAME: process.env.DB_NAME,
    USER_NAME: process.env.DB_USER_NAME, 
    PASSWORD: process.env.DB_PASSWORD, 
    HOST: process.env.DB_HOST, 
    DIALECT: 'mysql',
}

const STATUS = {
    OPEN: 'OPEN', 
    IN_PROGRESS: 'IN_PROGRESS', 
    COMPLETED: 'COMPLETED'
}

const DEFAULT_LIMIT = 10;
const DEFAULT_PAGE_NUMBER = 0;

module.exports = {
    PORT,
    DATABASE_CREDS,
    STATUS,
    DEFAULT_LIMIT,
    DEFAULT_PAGE_NUMBER,
}
