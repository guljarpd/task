const { constants } = require('../../configs');
const { taskModel, sequelize, QueryTypes } = require("../models");

/**
 * create task
 * @param {object} data 
 * @returns 
 */
const create = async (data) => {
  return await taskModel.create(data);
};

/**
 * Find task by task_id
 * @param {number} task_id 
 * @returns 
 */
const fetchByID = async (task_id) => {
  return await taskModel.findOne({ 
        where: { 
            task_id, 
            is_active: true 
        } 
    });
};

/**
 * Update task
 * @param {number} task_id 
 * @param {object} updateObj 
 * @returns 
 */
const update = async (task_id, updateObj) => {
    return await taskModel.update(updateObj, {where: {task_id}});
}

/**
 * Delete a task by task_id
 * @param {number} task_id 
 * @returns 
 */
const destroy = async (task_id) => {
  return await taskModel.destroy({
    where: {
      task_id
    }
  });
}

/**
 * Find all task by limit and offset for pagination
 * @param {number} offset 
 * @param {number} limit 
 * @returns 
 */
const fetchAll = async (offset, limit) => {
  const { count, rows } = await taskModel.findAndCountAll({
    where: {
      is_active: true 
    },
    offset,
    limit,
  });
  return {
    total_count: count,
    data: rows
  }
}

/**
 * Find the task metrics for analytics
 * @returns 
 */
const fetchMetrics = async () => {
  // custom query
  const queryString = 'SELECT DATE_FORMAT(`Task`.`created_datetime`, "%M %Y") AS `date`, SUM(CASE WHEN `Task`.`status` = "'+constants.STATUS.OPEN+'" THEN 1 ELSE 0 END) AS `open_tasks`, SUM(CASE WHEN `Task`.`status` = "'+constants.STATUS.IN_PROGRESS+'" THEN 1 ELSE 0 END) AS `inprogress_tasks`, SUM(CASE WHEN `Task`.`status` = "'+constants.STATUS.COMPLETED+'" THEN 1 ELSE 0 END) AS `completed_tasks` FROM `task` AS `Task` WHERE `Task`.`is_active` = true  GROUP BY `date`';
  return await sequelize.query(queryString, {type: QueryTypes.SELECT });
}

module.exports = {
  create,
  fetchByID,
  update,
  destroy,
  fetchAll,
  fetchMetrics,
};
