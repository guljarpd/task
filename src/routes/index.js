const { Task } = require('../controllers');

// Define all routes
module.exports = (app) => {
   app.post('/task/create', Task.createTask);
   app.get('/task/fetch', Task.fetchTask);
   app.get('/task/fetch/metrics', Task.fetchTaskMetrics);
   app.put('/task/update/:taskId', Task.updateTask);
   app.delete('/task/delete/:taskId', Task.deleteTask);
}