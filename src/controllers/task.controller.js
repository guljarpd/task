const { constants } = require('../../configs');
const { TaskService } = require('../services');

/**
 * Create a new task
 * @param {object} req - express request object
 * @param {object} req.body - express request body object
 * @param {string} req.body.task_name - required
 * @param {number} req.body.created_user_id - required
 * @param {string} req.body.status - optional {OPEN, IN_PROGRESS, COMPLETED}
 * @returns
 */
const createTask = async (req, res) => {
    try {
        console.log(createTask.name, 'Body=>', req.body);
        if (!req.body?.task_name || !req.body?.created_user_id) {
            console.error(createTask.name, 'Error=>', 'Insufficient params');
            return res.status(400).json({
                success: false,
                message: 'Insufficient params',
                params: ['task_name', 'created_user_id']
            }); 
        }
        const taskCreated = await TaskService.create(req.body);
        if (!taskCreated) {
            console.error(createTask.name, 'Error1=>', 'Task not created');
            return res.status(400).json({
                success: false,
                message: 'Task not created'
            });
        }
        return res.status(201).json({
            success: true,
            message: 'Successfully! Task created',
            data: taskCreated
        });
    } catch (error) {
        console.error(createTask.name, 'CatchError=>', error);
        return res.status(500).json({
            success: false,
            message: error.message
        });
    }
}

const fetchTask = async (req, res) => {
    try {
        console.log(fetchTask.name, 'query=>', req.query);
        const pageNumber = req.query?.page_number? parseInt(req.query.page_number) : constants.DEFAULT_PAGE_NUMBER;
        const limit = req.query?.limit? parseInt(req.query.limit)  : constants.DEFAULT_LIMIT;
        const offset = pageNumber? limit * (pageNumber -1) : pageNumber;
        const taskResult = await TaskService.fetchAll(offset, limit);
        if (!taskResult) {
            console.error(fetchTask.name, 'Error=>', 'Task not found');
           return res.status(404).json({
                success: false,
                message: 'Task not found'
            });
        }
        return res.status(200).json({
            success: true,
            message: 'Successfully! Task fetched',
            data: taskResult
        });
    } catch (error) {
        console.error(fetchTask.name, 'CatchError=>', error);
        return res.status(500).json({
            success: false,
            message: error.message
        });
    }
}

const fetchTaskMetrics = async (req, res) => {
    try {
        console.log(fetchTaskMetrics.name, 'Req=>');
        const metricsResults = await TaskService.fetchMetrics();
        const metricsData = metricsResults.map((item) => {
            return {
                date: item.date,
                metrics: {
                    open_tasks: item.open_tasks, 
                    inprogress_tasks: item.inprogress_tasks,
                    completed_tasks: item.completed_tasks,
                }
            }
        })
        return res.status(200).json({
            success: true,
            message: 'Successfully! Task metrics fetched',
            data: metricsData
        });
    } catch (error) {
        console.error(fetchTaskMetrics.name, 'CatchError=>', error);
        return res.status(500).json({
            success: false,
            message: error.message
        });
    }
}

const updateTask = async (req, res) => {
    try {
        console.log(updateTask.name, 'Params, Body=>', req.params, req.body);
        const taskId = req.params?.taskId;
        // check for task id
        if (!taskId) {
            console.error(updateTask.name, 'Error=>', 'Insufficient params');
            return res.status(400).json({
                success: false,
                message: 'Insufficient params',
                params: ['task_id']
            });
        }

        const updatedRes = await TaskService.update(taskId, req.body);
        if (!updatedRes) {
            throw 'Task not updated';
        }
        return res.status(200).json({
            success: true,
            message: 'Successfully! Task updated',
            data: updatedRes
        });
    } catch (error) {
        console.error(updateTask.name, 'CatchError=>', error);
        return res.status(500).json({
            success: false,
            message: error.message
        });
    }
}

const deleteTask = async (req, res) => {
    try {
        console.log(deleteTask.name, 'Params=>', req.params);
        const taskId = req.params?.taskId;
        // check for task id
        if (!taskId) {
            console.error(deleteTask.name, 'Error=>', 'Insufficient params');
            return res.status(400).json({
                success: false,
                message: 'Insufficient params',
                params: ['task_id']
            });
        }

        const updatedRes = await TaskService.destroy(taskId);
        if (!updatedRes) {
            throw 'Task not deleted';
        }
        return res.status(200).json({
            success: true,
            message: 'Successfully! Task deleted',
            data: updatedRes
        });
    } catch (error) {
        console.error(deleteTask.name, 'CatchError=>', error);
        return res.status(500).json({
            success: false,
            message: error.message
        });
    }
}


module.exports = {
    createTask,
    fetchTask,
    fetchTaskMetrics,
    updateTask,
    deleteTask,
}