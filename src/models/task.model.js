const { constants } = require('../../configs');
// Define Task Model
module.exports = (Sequelize, DataTypes) => {
    return Sequelize.define('Task', {
        task_id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        created_user_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        task_name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        status: {
            type: DataTypes.ENUM(constants.STATUS.OPEN, constants.STATUS.IN_PROGRESS, constants.STATUS.COMPLETED),
            defaultValue: constants.STATUS.OPEN,
            allowNull: false
        },
        is_active: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        created_datetime: {
            type: DataTypes.DATE,
            defaultValue: Sequelize.NOW
        },
        updated_datetime: {
            type: DataTypes.DATE,
            defaultValue: Sequelize.NOW
        }
    }, {
        tableName: 'task',
        timestamp: true,
        createdAt: 'created_datetime',
        updatedAt: 'updated_datetime',
    });
}