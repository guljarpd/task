const { Sequelize, DataTypes, QueryTypes } = require("sequelize");
const { constants } = require("../../configs");
const TaskModel = require("./task.model");

// db connection setup
const sequelize = new Sequelize(
  constants.DATABASE_CREDS.NAME,
  constants.DATABASE_CREDS.USER_NAME,
  constants.DATABASE_CREDS.PASSWORD,
  {
    host: constants.DATABASE_CREDS.HOST,
    dialect: constants.DATABASE_CREDS.DIALECT,
  }
);

const taskModel = TaskModel(sequelize, DataTypes);
// create table if not exist
sequelize.sync({ force: false });
module.exports = {
  sequelize,
  QueryTypes,
  taskModel,
};
