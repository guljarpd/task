# TaskApi

This is a CRUD application for task management.

### Setup and installation

---

Make sure you have `Nodejs v16.14 or above` installed on you system.

- create a `.env` file on root directory as per given example env file.
- replace the value with your MySQl database creds.
- install dependency with below command

```bash
    npm install
```

### How to run

---

- start test

```bash
    npm test
```

- start application

```bash
    npm run dev
```

### List of api

---

BaseUrl = http://localhost:8000

1.  Create task [/task/create]

    - Method -> `POST`
    - Curl request

    ```json
        curl --location '{BaseUrl}/task/create' \
        --header 'Content-Type: application/json' \
        --data '{
            "task_name": "Create a new database for customers",
            "status": "OPEN",
            "created_user_id": 1
        }'
    ```

    Response:

    ```json
    {
      "success": true,
      "message": "Successfully! Task created",
      "data": {
        "is_active": true,
        "created_datetime": "2023-09-28T12:03:43.221Z",
        "updated_datetime": "2023-09-28T12:03:43.221Z",
        "task_id": 37,
        "task_name": "Buy new home1",
        "status": "OPEN",
        "created_user_id": 1
      }
    }
    ```

2.  Fetch tasks with pagination [/task/fetch?page_number={pageNumber}&limit={limit}]

    - Method -> `GET`
    - Curl request

      ```json
          curl --location '{BaseUrl}/task/fetch?page_number=1&limit=3' \
          --data ''
      ```

      Response:

      ```json
      {
        "success": true,
        "message": "Successfully! Task fetched",
        "data": {
          "total_count": 50,
          "data": [
            {
              "task_id": 1,
              "created_user_id": 1,
              "task_name": "Buy new home",
              "status": "COMPLETED",
              "is_active": true,
              "created_datetime": "2022-07-27T11:18:34.000Z",
              "updated_datetime": "2023-09-27T11:19:24.000Z"
            },
            {
              "task_id": 2,
              "created_user_id": 1,
              "task_name": "Buy new home",
              "status": "OPEN",
              "is_active": true,
              "created_datetime": "2023-09-27T11:18:23.000Z",
              "updated_datetime": "2023-09-27T11:18:23.000Z"
            }
          ]
        }
      }
      ```

3.  Update task [/task/update/{task_id}]

    - Method -> `PUT`
    - Curl request

    ```json
        curl --location --request PUT '{BaseUrl}/task/update/5' \
        --header 'Content-Type: application/json' \
        --data '{
            "status": "IN_PROGRESS"
        }'
    ```

    Response:

    ```json
    {
      "success": true,
      "message": "Successfully! Task updated",
      "data": [1]
    }
    ```

4.  Delete task [/task/delete/{task_id}]

    - Method -> `DELETE`
    - Curl request

    ```json
        curl --location --request DELETE '{BaseUrl}/task/delete/2' \
        --data ''
    ```

    Response:

    ```json
    {
      "success": true,
      "message": "Successfully! Task deleted",
      "data": 1
    }
    ```

5.  Fetch task metrics [/task/fetch/metrics]

    - Method -> `GET`
    - Curl request

    ```json
        curl --location '{BaseUrl}/task/fetch/metrics' \
        --data ''
    ```

    Response:

    ```json
    {
      "success": true,
      "message": "Successfully! Task metrics fetched",
      "data": [
        {
          "date": "July 2022",
          "metrics": {
            "open_tasks": "0",
            "inprogress_tasks": "0",
            "completed_tasks": "1"
          }
        },
        {
          "date": "September 2023",
          "metrics": {
            "open_tasks": "47",
            "inprogress_tasks": "0",
            "completed_tasks": "0"
          }
        },
        {
          "date": "September 2022",
          "metrics": {
            "open_tasks": "0",
            "inprogress_tasks": "1",
            "completed_tasks": "0"
          }
        }
      ]
    }
    ```
